const express = require('express');
const router = express();

// Router endpoint
router.get("/", (req, res) => res.send("Welcome!"));

// Router endpoint to add two numbers
// NO LONGER IN USE. USE "/addition" enpoint instead

// router.get("/add", (req, res) => {
//     try {
//         const sum = Number(req.query.a) + Number(req.query.b);
//         res.send(sum.toString());
//     } catch(e){
//         res.sendStatus(500);
//     }
// });

// Router endpoint to multiply two numbers
router.get("/mul", (req, res) => {
    try {
        const product = Number(req.query.a) * Number(req.query.b);
        res.send(product.toString());
    } catch(e){
        res.sendStatus(500);
    }
});

// Router endpoint to add multiple numbers
router.get("/addition", (req, res) => {
    try{
        searchParams = new URLSearchParams(req.query);
        const values = searchParams.values();
        let sum = 0;
        for(let p of values){
            sum += Number(p);
        }
        res.send(sum.toString());
    } catch(e){
        res.sendStatus(500);
    }
});

// Endpoint 3 POST http://localhost:300/webhook-update
/** Proper JSON REST API Middlewares */
router.use(express.urlencoded({extended: true}));
router.use(express.json());
// post here

module.exports = router;