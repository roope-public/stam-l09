# Licensing and versioning - Roope Salminen

## Task 1:

The start version for the latest project (stam-l09) can be found in the file `package.json`. We see that it was automatically set to 1.0.0.

1. New feature is added to the project without breaking backwards compatibility
    - This is a minor update so the next version number is set to 1.1.0
2. Bug is fixed without changing the API
    - This is considered a bug fixing patch so the version number is now increased from 1.1.0 to 1.1.1
3. Change some API in a way it breaks backwards compatibility
    - A major update that breaks backwards compatibility increses the version number from 1.1.1 to 2.0.0

## Task 2:


Copyright 2023 Roope Salminen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## Task 3:

Repository can be found [here](https://gitlab.com/roope-public/stam-l09).